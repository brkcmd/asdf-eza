<div align="center">

# asdf-eza ![Build Status](https://gitlab.com/brkcmd/asdf-eza/badges/trunk/pipeline.svg)

[eza](https://github.com/eza-community/eza) plugin for the [asdf version manager](https://asdf-vm.com).

</div>

# Contents

- [Dependencies](#dependencies)
- [Install](#install)
- [Contributing](#contributing)
- [License](#license)

# Dependencies

- `bash`, `curl`, `tar`: generic POSIX utilities.

# Install

Plugin:

```shell
asdf plugin add https://gitlab.com/brkcmd/asdf-eza.git
```

`eza`:

```shell
# Show all installable versions
asdf list-all eza

# Install specific version
asdf install eza latest

# Set a version globally (on your ~/.tool-versions file)
asdf global eza latest

# Now eza commands are available
eza
```

Check [asdf](https://github.com/asdf-vm/asdf) readme for more instructions on how to
install & manage versions.

# Contributing

Contributions of any kind welcome! See the [contributing guide](contributing.md).

[Thanks goes to these contributors](https://gitlab.com/brkcmd/asdf-eza/-/graphs/trunk)!

# License

See [LICENSE](LICENSE) © [brkcmd](https://gitlab.com/brkcmd/)

# Contributing

Testing (with `bash`):

```shell
asdf plugin test eza ./.git --asdf-plugin-gitref "$(git rev-parse --abbrev-ref HEAD)" -- "eza --version"
```

Tests are automatically run in GitLab CI on push and merge request.
